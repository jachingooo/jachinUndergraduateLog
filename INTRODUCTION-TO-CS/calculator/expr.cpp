#include "expr.h"
#include "ui_expr.h"

expr::expr(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::expr)
{
    ui->setupUi(this);
    this->mode=new model;
    this->tmp="";
}

expr::~expr()
{
    delete ui;
}

void expr::on_btn_0_clicked()
{
    if(this->tmp !=""){
        this->tmp += this->ui->btn_0->text();
        this->ui->lcd_display->display(this->tmp);
    }
}

void expr::on_btn_c_clicked()
{
    this->tmp = "";
    this->ui->lcd_display->display(0);
}

void expr::on_btn_div_clicked()
{
    bool ok;
    int num = this->tmp.toInt(&ok);
    this->mode->setNum1(num);
    this->tmp = "";
    QString ex = this->ui->btn_div->text();
    this->mode->setFlag(ex);
}

void expr::on_btn_mul_clicked()
{
    bool ok;
    int num = this->tmp.toInt(&ok);
    this->mode->setNum1(num);
    this->tmp = "";
    QString ex = this->ui->btn_mul->text();
    this->mode->setFlag(ex);
}

void expr::on_btn_1_clicked()
{
this->tmp += this->ui->btn_1->text();
this->ui->lcd_display->display(this->tmp);
}

void expr::on_btn_2_clicked()
{
    this->tmp += this->ui->btn_2->text();
    this->ui->lcd_display->display(this->tmp);
}

void expr::on_btn_3_clicked()
{
    this->tmp += this->ui->btn_3->text();
    this->ui->lcd_display->display(this->tmp);
}

void expr::on_btn_sub_clicked()
{
    bool ok;
    int num = this->tmp.toInt(&ok);
    this->mode->setNum1(num);
    this->tmp = "";
    QString ex = this->ui->btn_sub->text();
    this->mode->setFlag(ex);
}

void expr::on_btn_4_clicked()
{
    this->tmp += this->ui->btn_4->text();
    this->ui->lcd_display->display(this->tmp);
}

void expr::on_btn_5_clicked()
{
    this->tmp += this->ui->btn_5->text();
    this->ui->lcd_display->display(this->tmp);
}

void expr::on_btn_6_clicked()
{
    this->tmp += this->ui->btn_6->text();
    this->ui->lcd_display->display(this->tmp);
}

void expr::on_btn_plus_clicked()
{
       bool ok;
       int num = this->tmp.toInt(&ok);
       this->mode->setNum1(num);
       this->tmp = "";
       QString ex = this->ui->btn_plus->text();
       this->mode->setFlag(ex);
}

void expr::on_btn_7_clicked()
{
    this->tmp += this->ui->btn_7->text();
    this->ui->lcd_display->display(this->tmp);
}

void expr::on_btn_8_clicked()
{
    this->tmp += this->ui->btn_8->text();
    this->ui->lcd_display->display(this->tmp);
}

void expr::on_btn_9_clicked()
{
    this->tmp += this->ui->btn_9->text();
    this->ui->lcd_display->display(this->tmp);
}

void expr::on_btn_equal_clicked()
{
    bool ok;
    int num = this->tmp.toInt(&ok);
    this->mode->setNum2(num);
    QString res = this->mode->doExpr();
    this->ui->lcd_display->display(res);
    this->tmp = "";
}
